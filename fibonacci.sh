#Program for Fibonacci series in words 
   
read N  
# First Number of the 
# Fibonacci Series 
a=0 
  
# Second Number of the 
# Fibonacci Series 
b=1  
   
echo "The Fibonacci series is : "
   
for (( i=0; i<N; i++ )) 
do
    echo -n "$a " | ./numbers_to_words.sh
    fn=$((a + b)) 
    a=$b 
    b=$fn 
done
