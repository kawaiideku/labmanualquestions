#Shell script to enter an integer and print words for each digit
#!/bin/sh

read n
len=$(echo $n | wc -c)
len=$(( $len - 1 ))
 
for (( i=1; i<=$len; i++ ))
do
   #get one digit at a time
    digit=$(echo $n | cut -c $i)
    case $digit in
        0) echo -n "zero " ;;
        1) echo -n "one " ;;
        2) echo -n "two " ;;
        3) echo -n "three " ;;
        4) echo -n "four " ;;
        5) echo -n "five " ;;
        6) echo -n "six " ;;
        7) echo -n "seven " ;;
        8) echo -n "eight " ;;
        9) echo -n "nine " ;;
    esac 
done
 
echo ""
