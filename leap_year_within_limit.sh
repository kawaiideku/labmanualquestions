#shell script to input 2 limits and display all leap years in between
read -p "Enter lower limit : " low
read -p "Enter upper limit : " upp

for (( i=low; i<=$upp; i++ ))
do
	a=$(( $i % 4 ))
	if [ $a -eq 0 ]
	then
		b=$(( $i % 100 ))
		if [ $b -eq 0 ]
		then
			c=$(( $i % 400))
			if [ $c -eq 0 ]
			then
				echo $i | ./numbers_to_words.sh
			fi
		else
			echo $i | ./numbers_to_words.sh
		fi
	fi
done
